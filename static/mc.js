var webPage = require('webpage');
var system = require('system');

if (system.args.length === 1) {
    phantom.exit(1);
} else {
    adres = system.args[1];

}

var ayc = String.fromCharCode(30);

var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36';
page.settings.resourceTimeout = 20000;
page.viewportSize = {width: 1366, height: 1600};


page.onError = function (msg, trace) {
    var msgStack = ['ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function (t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }
    // uncomment to log into the console
    //console.error("HATALAR" + msgStack.join('\n'));
};

page.open(adres, function (status) {
    if (status !== "success") {
        console.log("ERROR");
        phantom.exit();
    } else {

        var links = page.evaluate(function () {

            alar = document.querySelectorAll('a');
            total = "";
            for (i = 0; i < alar.length; i++) {
                if (alar[i].text != null)
                    total += alar[i].text.trim() + String.fromCharCode(29);
            }
            return total;
        });


        desc = page.evaluate(function () {
            s = document.querySelector("meta[name='description'],meta[name='Description'],meta[name='DESCRIPTION']");
            if (s)
                return s['content'];
        });

        key = page.evaluate(function () {
            s = document.querySelector("meta[name='keywords'],meta[name='Keywords'],meta[name='KEYWORDS'],meta[name='keyword']");
            if (s)
                return s['content'];
        });

        sonuc = page.url + ayc;
        sonuc += page.title + ayc;
        sonuc += desc + ayc;
        sonuc += key + ayc;
        sonuc += page.plainText + ayc;
        sonuc += links + ayc;
        sonuc += page.url + ayc;

        console.log(sonuc);

        phantom.exit();
    }
});

