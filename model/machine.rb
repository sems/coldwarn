class Machine < Sequel::Model
  one_to_many :states,:key=>:machine_id
  def before_create
    self.created_at ||= Time.now
    super
  end
end