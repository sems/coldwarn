class State < Sequel::Model
  many_to_one :machine,:key=>:machine_id

  def validate
    super
    errors.add(:machine_id, 'cannot be empty') if !machine_id
  end

  def before_create
    self.created_at ||= Time.now
    super
  end


end