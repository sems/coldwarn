class Pi
  def self.ip
    return `ifconfig eth0 | grep "inet addr" | awk -F'[:B]' '{print $2}'`.strip
  end

  def self.internet
    s= `wget http://www.msftncsi.com/ncsi.txt -qO -`.strip
    if s=="Microsoft NCSI"
      return true
    else
      return false
    end
  end

  def self.public_ip
    return `wget http://ipinfo.io/ip -qO -`.strip
  end
end
