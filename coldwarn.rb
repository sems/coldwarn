require_relative "helper"
require "daemons"


Daemons.run_proc(
    'Net Check', # name of daemon
    #  :dir_mode => :normal
    :dir => "#{$pwd}/tmp", # directory where pid file will be stored
    :multiple => false,
    :backtrace => true,
    :monitor => true,
    :log_output => true,
    :logfilename => "#{$pwd}/log/application.log",
    :output_logfilename => "#{$pwd}/log/application.log"
) do
  require_relative "service_scripts/net_check"
end


Daemons.run_proc(
    'Dummy', # name of daemon
    #  :dir_mode => :normal
    :dir => "#{$pwd}/tmp", # directory where pid file will be stored
    :backtrace => true,
    :monitor => true,
    :multiple => false,
    :log_output => true,
    :logfilename => "#{$pwd}/log/application.log",
    :output_logfilename => "#{$pwd}/log/application.log"
) do
   require_relative "service_scripts/dummy"
end


# Daemons.run('service_scripts/dummy.rb',o )
# Daemons.run('service_scripts/net_check.rb', )
