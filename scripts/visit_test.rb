#url_list=File.readlines("data/back_feed_total.txt")[0..10000]


# Parallel.each(url_list, :in_threads => 8, :progress => "Liste giriliyor") do |u|
#   s=State.new
#
#   u=u.gsub("\\n", "")
#   u=u.gsub("\n", "")
#    begin
#     s.save if s.yeni?(u)
#   rescue Exception => e
#     p e.backtrace.join("\n")
#     p "#{u} atlandi"
#   end
# end


data=State.where(try_date: nil).limit(10000).to_a
Parallel.each(data, :in_threads => 8) do |s|

  begin
    p s.visit
    s.save
    p s.summary


  rescue Exception => e
    LOG.error "Paralel hatasi #{self.url}: #{e.backtrace.join(" - ")}"
  end
end