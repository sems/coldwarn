`cd #{$pwd}; bundle install`
`mkdir #{$pwd}/tmp`
`mkdir #{$pwd}/log`

require_relative "../helper"

APP_NAME = 'ColdWarn'
APP_PATH = "#{$pwd}/coldwarn.rb"

metin="#!#{`which ruby`.strip}


case ARGV.first
  when 'start'
    puts \"Starting #{APP_NAME}...\"
    print `ruby #{APP_PATH} start`
  when 'stop'
   print `ruby #{APP_PATH} stop`
  when 'restart'
    print `ruby #{APP_PATH} restart`
 when 'status'
    print `ruby #{APP_PATH} status`
end

unless %w{start stop restart status}.include? ARGV.first
  puts \"Usage: #{APP_NAME} {start|stop|restart|status}\"
  exit
end
"
File.write("#{$pwd}/init.rb", metin)
`sudo rm /etc/init.d/#{APP_NAME}`
`sudo mv #{$pwd}/init.rb /etc/init.d/#{APP_NAME}`
`sudo chown root:root /etc/init.d/#{APP_NAME}`
`sudo chmod +x /etc/init.d/#{APP_NAME}`
`sudo update-rc.d #{APP_NAME} defaults`