#DB.drop_table(:categories)
class Setup

  def self.create

    DB.create_table?(:machines, :charset => "utf8") do
      primary_key :id
      DateTime :created_at
      # Bignum :class_id
      String :name, :size => 100
      String :port, :size => 50
      String :description, :size => 250
    end

    DB.create_table?(:states, :charset => "utf8") do
      primary_key :id
      foreign_key :machine_id, :machines
      # String :url, :size => 250
      # String :clean_url, :size => 250
      DateTime :created_at
      # DateTime :try_date
      # DateTime :visit_date
      # String :url_visited, :size => 250
      # String :language, :size => 10
      # String :domain_name, :size => 100
      String :summary, :text => true
      # String :links, :text => true
      Bignum :temp
      # FalseClass :confirmed
    end

  end

  def self.db(ez)
    if ez==true
      DB.drop_table?(:machines, :states)
    end
    create
    return "restart app"
  end


end